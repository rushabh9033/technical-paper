# **Java Script ES6**

## **Introduction**

JavaScript is a cross-platform, object-oriented scripting language used to make web pages interactive. JavaScript is a living language that is constantly adding new features.

JavaScript is standardized at Ecma International — the European association for standardizing information and communication systems to deliver a standardized, international programming language based on JavaScript. This standardized version of JavaScript is called ECMAScript, which was invented by Brendan Enrich in 1995, and became an ECMA standard in 1977.

There are many versions available. So, JavaScript is known as the official ECMAScript. The first edition of ECMAScript is known as ES1. Afterward, many versions were updated, such as ES1, ES2, ES3, ES4, ES5, ES6, and so on. In this version, there is no more feature upgrade, but in ES6, there were major changes which I described below, and also ES6 is compatible with more browsers. In addition, ES6 has many built-in methods, built-in functions, and a library.

## **Variables**

In ES5, there is only one keyword used for variables defined which is var. When you use this keyword to declare a variable, you sometimes get an error in the function scope. So, in ES6, there are two keywords used for variable declarations which are let and const.

```javascript
let first_name = "rushabh";
let a = 6;
const arr = [1, 2, 3, 4];
```

The difference is that with const you can only assign a value to a variable once, but with let it allows you to reassign after it has been assigned.

## **Data Types**

JavaScript provides different data types to hold different types of values. There are two types of data types in JavaScript.

1. ### Primitive Data Types

1. ### Non-primitive Data Types

- ### Primitive data types

  There are six types of primitive data types.

  1.  String
  2.  Boolean
  3.  Number
  4.  Null
  5.  Undefined
  6.  Symbol
  7.  BigInt

- ### Non-primitive data types

  There are two types of non-primitive data types.

  1. Array
  2. Object

### Primitive data types

**1. String**

This data type is used to define some non-number value like any alphabetical value. We use a double comma " " or single comma ' ' for declaration.

```javascript
let s = "abc";
let name = "rushabh";
const full_name = "navapara rushabh";
```

**2. Boolean**

This data type is used to declare a boolean value that is **true** or **false**. This data type is needed when you have to get a result that is either true or false.

```javascript
let a = true;
let b = false;
let value = true;
```

**3. Number**

The number type represents both integer and floating-point numbers.

```javascript
let n = 21;
cont b = 2333;
let a = 21.5;
```

**4. Null**

This is a special type of its own which contains only the null value. In JavaScript, null is not a “reference to a non-existing object” or a “null pointer” like in some other languages. It’s just a special value that represents “nothing”, “empty” or “value unknown”.

```javascript
let age = null;
let b = null;
```

**5. Undefined**

The special value undefined also stands out. The meaning of undefined is “value is not assigned”. If a variable is declared, but not assigned, then its value is undefined.

```javascript
let age;

alert(age); // shows "undefined"
```

**6. Symbol**

The JavaScript ES6 introduced a new primitive data type called Symbol. Symbols are immutable (cannot be changed) and are unique.

```javascript
const value1 = Symbol("hello");
const value2 = Symbol("hello");

console.log(value1 === value2); // false
```

**7. BigInt**

If you have to assign a large value, the BigInt data type comes in handy. This data type is used to represent large values like (2<sup>53</sup> - 1).

```javascript
const bigInt = 45465465465464544544546545621281269615616n;
```

### Non-primitive data types

**1. Array**

Array is use to store the different data type of value. Array can store any value. An array in JavaScript can be defined and initialized in two ways, array literal and Array constructor syntax.

```javascript
var stringArray = ["one", "two", "three"];

var numericArray = [1, 2, 3, 4];

var decimalArray = [1.1, 1.2, 1.3];

var booleanArray = [true, false, false, true];

var mixedArray = [1, "two", "three", 4];

//Declare array using array constructor
let arr = new Array(true, "rushabh", 3);
```

**2. Object**

A JavaScript object is an entity having state and behavior (properties and method). For example car, pen, bike, chair, glass, keyboard, monitor, etc. Basically, the object is a key-value pair.

```javascript
let user_info = {
    fname: "rushabh",
    id: 40,
    lname: "navapara"
    salary: 40000
}

```

## **Arrow Function**

Arrow functions are one of the best features introduced in ES6. They introduce a new way of writing concise functions. Arrow functions allow a short syntax for writing function expressions. This function allows you don't need the function keyword, the return keyword, and the curly brackets. Syntax of arrow function given below.

```javascript
const squarFn = (num) => {
  return num * num;
};
const result = squarFn(12);
console.log(result); // print the 144
```

## **Default parameter**

The default parameter is also one of the best features which are introduced in ES6. It allows you to set default values for your function parameters if no value is passed or if undefined is passed. First, what happens when no parameter is passed to a function that requires parameters.

```javascript
function add(a = 5, b = 3) {
  return a + b;
}
add(4, 2); // 6
add(5); // 8
add(); // 8
```

It is important to note that parameters are set from left to right. So the overwriting of default values will occur based on the position of your input value when you call the function. For instance, in our last example, when one parameter was passed add(5) since 5 was passed first, it was automatically assumed to be a.

## **Template Literals**

Template literals are a new feature introduced in ES6. It provides an easy way to create multiline strings and perform string interpolation. Template literals are the string literals and allow embedded expressions. Template literals are enclosed by the backtick (``). Template literals can contain placeholders, which are indicated by the dollar sign and curly braces ($(expression}).

```javascript
let name = "World";
let cname = "My technical paper";
console.log(`Hello, ${name}!  Welcome to ${cname}`);
```

## **Destructuring assignment**

The two most used data structures in JavaScript are Object and Array. Objects allow us to create a single entity that stores data items by key. Arrays allow us to gather data items into an ordered list. If we pass those to a function, it doesn't require an object or array as a whole. It may need individual pieces. So here, destructuring assignment concepts come. Destructuring assignment is a special syntax that allows us to “unpack” arrays or objects into a bunch of variables, as sometimes that’s more convenient.

```javascript
const [greeting, pronoun] = ["Hello", "I", "am", "Sarah"];

console.log(greeting); //"Hello"
console.log(pronoun); //"I"
```

## **Spread operator**

ES6 provides a new operator called the spread operator that consists of three dots (...). The spread operator allows you to spread out elements of an iterable object such as an array, a map, or a set. The spread operator is used for the combination of multiple arrays.

```javascript
const arr = [1, 3, 5];
const combined = [2, 4, 6, ...arr];
console.log(combined); //[2,4,6,1,3,5]
```

## **Rest Parameter**

The rest parameter allows us to represent an indefinite number of arguments as an array. By using the rest parameter, a function can be called with any number of arguments. The rest parameter is prefixed with three dots (...). Although the syntax of the rest parameter is similar to the spread operator, it is entirely opposite from the spread operator.

```javascript
function sum(...args) {
  let total = 0;
  for (let i of args) {
    total += i;
  }
  return total;
}
console.log(sum(1, 2, 3, 4, 5));
```

## **Reference**

[Java Script Info](https://javascript.info/)

[MDN Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

[Dev Community](https://dev.to/)
